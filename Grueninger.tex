\documentclass[ngerman]{dtk}
\ifluatex
\else
  \usepackage[utf8]{inputenc}
\fi
\usepackage{amsmath}

\addbibresource{Grueninger.bib}

\begin{document}

%opening
\title{Grafische Darstellung dünnbesetzter Matrizen}
\Author{Christoph}{Grüninger}%
       {Sindelfingen\\
        \Email{foss@grueninger.de}}
\maketitle

\begin{abstract}
Große, dünnbesetzte Matrizen treten in verschiedenen Feldern der Mathematik
und ihren Anwendungen auf, zum Beispiel in der Numerik, um lineare Gleichungssysteme
zu beschreiben. Die Matrizen sind zu groß, um abgedruckt oder von Menschen
in dieser Form erfasst zu werden. Die konkreten Werte treten in den Hintergrund,
es geht stattdessen um die Struktur und die Größe von Nichtnulleinträgen.
Mit wenigen Zeilen \TikZ{} kann man aus externen Dateien grafische Darstellungen
großer, dünnbesetzter Matrizen erzeugen.
\end{abstract}

% \begin{abstract}
% Large sparse matrices occur in various areas of mathematics and its
% applications, i.e., in numerics to describe systems of linear equations.
% The matrices are too large to be printed or that humans can apprehend
% them. The actual values take a back seat. Of more importance becomes
% the structure and the value of non-zero entries.
% With a couple of lines of TikZ code, we create visualisations of large
% sparse matrices.
% \end{abstract}

Eine Matrix bezeichnet in der Mathematik eine Tabelle, die mit Zahlen oder sonstigen mathematischen Objekten gefüllt ist. Eine $n\times m$-Matrix besteht aus $n$ Zeilen
und $m$ Spalten. Wir gehen im Folgenden von quadratischen Matrizen aus, also $n=m$.
Mit \LaTeX{} kann eine Matrix unter anderem mit dem Befehl
\lstinline{pmatrix}
dargestellt werden, zum Beispiel ergibt im Mathematikmodus
\begin{lstlisting}
\begin{pmatrix}
 1 & 0 \\
 3{,}65 & -4
\end{pmatrix} 
\end{lstlisting}
die Matrix
\begin{equation*}
 \begin{pmatrix}
  1 & 0 \\
  3{,}65 & -4
 \end{pmatrix}.
\end{equation*}

Wenn bei einer $n \times n$-Matrix nur $c \cdot n$ Einträge nicht null sind für
eine Konstante $c$ -- in Big-O-Notation $\mathcal{O}(n)$ --
spricht man von einer dünnbesetzten Matrix. Die Verteilung der Nichtnulleinträge in
der Matrix, die Besetzungsstruktur, gibt Hinweise auf deren Eigenschaften wie
Symmetrie, Bandstrukturen oder Möglichkeiten das dahinterstehenden lineare
Gleichungssystem effizient zu lösen.

Unter anderem
in der numerischen Mathematik treten bei Näherungsverfahren für
partielle Differentialgleichungen, wie der Finite-Elemente-Methode, solche großen
dünnbesetzten Matrizen auf. Es gibt Sammlungen von dünnbesetzten Matrizen, um
Lösungsverfahren an Matrizen aus der Praxis zu testen und miteinander zu vergleichen.
Vom NIST wurden bis 2007 fast 500 Matrizen im Matrix Market gesammelt \cite{matrixMarket},
die SuiteSparse Matrix Collection hat mehr als 2800 dünnbesetzte Matrizen aus
den letzten 50 Jahren zusammengetragen \cite{davisSparsePaper2011, davisSparseWebsite}.

\section{Besetztheitsstruktur}
Matrizen ab einer Größe von 25 Zeilen und Spalten können nicht mehr wie oben
abgedruckt oder angezeigt werden. Auch kann ein Mensch so viele Zahlen nicht
mehr erfassen und interpretieren. Mit einer grafischen Darstellung kann man
dies für größere Matrizen erreichen.

Die einfachste grafische Darstellung der Besetztheitsstruktur ist die Reduktion
der Nichtnulleinträge als Kreis oder Quadrat und der Nulleinträge als leere Stelle.
Matlab und die Python-Bibliothek matplotlib bieten mit dem Befehl \lstinline{spy} eine entsprechende Visualisierungsfunktion.

Abbildung~\ref{fig:spy} zeigt beispielhaft eine Matrix aus einer Finite-Differenzen-Diskreti\-sier\-ung
eines $4\times 4$-Gitters. Man erkennt die charakteristische Struktur der Matrix:
Komplett gefüllte Diagonale und zwei Nebendiagonalen im Abstand
von vier. Dazu die direkten Nebendiagonalen mit Abschnitten der Länge vier Einträgen. 

\begin{figure}
  \centering
  \includegraphics{data/img_spy.pdf}
  \caption{Matrix-Darstellung im Stile von \lstinline{spy}. Nichtnulleinträge
           sind als Kästen dargestellt, Nullen als Leerstelle.}
  \label{fig:spy}
\end{figure}

Die Darstellung im Stil von \lstinline{spy} können wir mit knapp 20 Zeilen
\TikZ{}-Code erzeugen:

\lstinputlisting{data/spy.tex}

Die Matrix muss als eigene Datei im Matrix-Market-Format vorliegen, wie in
\cite{matrixMarketFormat}
beschrieben. Am Dateianfang können beliebig viele Kommentarzeilen stehen, die mit \%
beginnen. Dann kommt eine Zeile, in der mit Leerzeichen getrennt drei Zahlen stehen:
Die Zahl der Matrixzeilen, die Zahl der Matrixspalten und die Zahl der Nichtnulleinträge. Danach
folgen für jeden Nichtnulleintrag wieder drei mit Leerzeichen getrennte Zahlen:
Die Spalte, die Zeile und der Wert des Nichtnulleintrags. Die ersten Zeilen der
Matrix-Market-Datei der abgebildeten Matrix lauten:

\lstinputlisting[firstline=6,lastline=12]{data/spy.mtx}

Die Matrixdatei wird mit \lstinline{pgfplotstable} eingelesen und aus der ersten
Zeile die Matrixgröße und die Anzahl der Nichtnulleinträge ermittelt. Letztere
ist gleichzeitig die Zahl der verbleibenden Zeilen der Datei. Mit der
\lstinline{foreach}-Schleife zeichnen wir für jeden
Nichtnulleintrag ein graues Quadrat gemäß der angegebenen Spalte und Zeile.

\section{Mehr Individualität}
Wenden wir uns einer spannenderen Matrix zu und visualisieren einen
Ausschnitt aus der Matrix \textit{windtunnel\_evap2d} von
\cite{davisSparseWebsite} (ID 2814). Es handelt sich
um eine Diskretisierung einer nicht-isothermen Navier-Stokes-Gleichung
mit Transport einer Komponente auf $4 \cdot 4$ Zellen, also
einem System von partiellen Differentialgleichungen.
Die darzustellende $88 \times 88$-Matrix enthält 772
Nichtnulleinträge.

Die rudimentäre Darstellung aus dem letzten Abschnitt ist ein guter
Ausgangspunkt für Anpassungen an den eigenen Bedarf. Zum Beispiel
können wir die Nichtnulleinträge abhängig von deren Größe einfärben.
Da sich die Einträge über mehrere Größenordnungen erstrecken, soll der
Logarithmus vom Betrag der Einträge verwendet und mit einer
Colormap eingefärbt werden. Außerdem sollen Blöcke entsprechend ihrer
physikalischen Bedeutung im Gleichungssystem beschriftet werden. Weiter
sollen zwei charakteristische Bereiche hervorgehoben werden: Mit einem
Quadrat soll ein Block auf der Diagonalen markiert werden, der
ausschließlich Nulleinträge enthält und die Lösung des Gleichungssystems
erschwert. Mit einer Ellipse sollen große Nichtnulleinträge
eingekreist werden, die weit abseits der Diagonale liegen und sich
ebenfalls negativ auf viele Lösungsverfahren auswirken.

\begin{figure}
  \centering
  \includegraphics{data/img_windtunnel.pdf}
  \caption{Größere Matrix mit eingefärbten Nichtnulleinträgen abhängig
           vom Betrag des Eintrags, mit zusätzlicher Beschriftung und
           der Hervorhebung zweier Besonderheiten der Matrix}
  \label{fig:windtunnel}
\end{figure}

Die entsprechende Darstellung ist Abbildung~\ref{fig:windtunnel}, welche
aus 50 Zeilen \TikZ{}-Code entsteht:

\lstinputlisting{data/windtunnel.tex}

In den ersten beiden Zeilen wird die Colormap Viridis ausgewählt und in
einem Stil definiert, der dann für jeden Nichtnulleintrag weiter unten
verwendet werden kann. Viridis ist den klassischen Colormaps wie Jet
vorzuziehen, weil es den Farbraum gleichmäßig durchläuft, somit in
Graustufen die Aussage nicht verfälscht und darüber hinaus für
Farbenblinde geeignet ist \cite{rainbowHarmful,viridis}.

Das Einlesen der Matrix und die Iterationen über die Nichtnulleinträge
sind wie beim ersten Beispiel. Erst die Berechnung des Farbwertes ist neu. Hierfür
wird der Logarithmus des Betrags linear auf einen Farbindex zwischen
1 und 1000 abgebildet, die Konstanten 44 und 21 sind händisch an die Matrixextremwerte
angepasst, um das Beispiel nicht weiter mit einer Berechnung zu verkomplizieren.
Die Beschriftung des Koordinatensystems gibt den Namen der Unbekannten
aus der jeweiligen Gleichung an. Die Legende hilft bei der Zuordnung
der Farben zu der Größe der Matrixeinträge. Die beiden hervorgehobenen
Matrixeigenschaften sind direkt in die Grafik eingefügt, es steht also
das gesamte Repertoire an \TikZ{} für solche Ergänzungen zur Verfügung.

Einige Dinge könnten noch verbessert werden:
Das Koordinatensystem, die Berechnung des Colorindex und die
Hervorhebungen in der Matrix könnten automatisch berechnet werden, anhand
von wenigen zu definierenden Konstanten. 
Die Legende könnte in einen Streifen mit dem Durchlauf durch die Colormap
verändert werden, in dem ausgewählte Werte gekennzeichnet sind.

\section{Einordnung}
Beide Beispiele zeigen primär die Mächtigkeit von \TikZ{} und
\lstinline{pgfplotstable}. Jeder Aspekt der Matrix-Darstellung kann verändert
werden und auch Spezialfälle wie vergrößerte Details, Auslassungen
oder die Anreicherungen mit weiteren Darstellungen lassen sich umsetzen.

Der gewählte Ansatz zur Darstellung großer Matrizen hat selbstverständlich seine Grenzen. Beim Versuch die
gesamte $8256 \times 8256$-Matrix windtunnel\_evap2d mit fast 110\,000
Nichtnulleinträgen darzustellen, ging \pdfLaTeX{} mit Standardeinstellungen der Speicher aus. Generell
dauert der Compilevorgang lange und nervt schnell beim Arbeiten. Man sollte
die Grafiken auslagern, um normal mit dem umgebenden Dokument weiterarbeiten
zu können.

\LaTeX{} wäre nicht \LaTeX{}, wenn man Matrizen nicht auch mit Bibliotheken
formschön darstellen könnte. PGFPlots bietet mit
\lstinline{matrix plot} entsprechende Möglichkeiten. Wer PSTricks mag, kann mit pstricks-add
den Befehl \lstinline{psMatrixPlot} für die Darstellung von Matrizen verwenden.
Die obigen \TikZ{}-Beispiele sind hoffentlich trotzdem lehrreich -- und manchmal möchte
man einfach die volle Kontrolle behalten.

Die Möglichkeiten, die sich durch eine konsequente
Anwendung von aus Quelldaten erstellen Grafiken ergeben, sind nicht zu
unterschätzen. Oft vergisst man nach wenigen Wochen, was man alles mit den
Daten gemacht hat, um eine Grafik zu erhalten. Hier müsste man einfach die
Matrixdatei austauschen und wäre nach einem Aufruf von \LaTeX{} fertig.

Auch bei der Reproduzierbarkeit
von wissenschaftlichen Arbeiten hilft die Vorgehensweise. Man könnte zum Beispiel
für einen Artikel aus der Numerik den Quelltext der Berechnung zusammen mit dem
\LaTeX{}- und \TikZ{}-Quellen veröffentlichen. Jeder Interessierte könnte
nicht nur die Berechnung wiederholen, sondern könnte auch die Auswertung
der Daten nachvollziehen und den Artikel komplett reproduzieren. Manipulationen, Auslassung von Daten oder eine
geschickte Wahl von Datenpunkte wären leichter zu entdecken.

\printbibliography

\end{document}
