raw = load('windtunnel_evap2d.mtx_orig');
matrix = spconvert(raw);

nachv1 = 32 * 33;
nachv2 = nachv1 + 33 * 32;
nachp = nachv2 + 32 * 32;
nachT = nachp + 32 * 32;

selectorv1 = [1:5 1+32:5+32 1+64:5+64 1+96:5+96];
selectorv2 = [1:4 1+32:4+32 1+64:4+64 1+96:4+96 1+128:4+128];
selector = [1:4 1+32:4+32 1+64:4+64 1+96:4+96];
selection = matrix([selectorv1 nachv1+selectorv2 nachv2+selector nachp+selector nachT+selector],
                   [selectorv1 nachv1+selectorv2 nachv2+selector nachp+selector nachT+selector]);
[i,j,val]=find(selection);
dlmwrite('windtunnel.mtx',[size(selection) nnz(selection);i j val],'delimiter', ' ','newline','pc');
